package main

import (
	"net/url"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// IsValidURL checks if u is a valid URL string with url.Parse
func IsValidURL(u string) (bool, error) {

	_, err := url.Parse(u)
	if err != nil {
		return false, err
	}

	return true, nil
}

// CreateAppConfig populates a global application configuration struct and returns a pointer to it
func CreateAppConfig() *AppConfig {

	var err error

	registerConfigVariables()

	config := &AppConfig{}

	// populate config struct with initial static config values
	initialConfig(config)

	err = setupClusterPeers(config)
	if err != nil {
		config.UseClusterMode = false
	} else {
		config.UseClusterMode = true
		// test connectivity
	}

	config.LocalCache = newDefaultCache(config)

	return config
}

func setupLog() {
	logger = &log.Logger{
		Out:       os.Stdout,
		Hooks:     make(log.LevelHooks),
		Formatter: new(log.JSONFormatter),
		Level:     log.DebugLevel,
	}
}

func registerConfigVariables() {
	// Set default values for every config option but secrets
	viper.SetDefault("clientId", "kube-stg")
	viper.SetDefault("providerURL", "https://oidc.eexample.org")
	viper.SetDefault("callbackURL", "http://127.0.0.1:8000/auth/dex/callback")
	viper.SetDefault("listenAddress", ":8000")
	viper.SetDefault("kubeCACertData", "XXX")
	viper.SetDefault("kubeAPIAddress", "https://k8s.example.org")
	viper.SetDefault("kubeContextName", "k8s")
	viper.SetDefault("kubeClusterName", "kube_cluster")
	viper.SetDefault("KubeUserPrefix", "oidc:")
	viper.SetDefault("logLevel", "Info")
	viper.SetDefault("clusterSelfURL", "http://localhost")
	viper.SetDefault("clusterDiscoveryScheme", "http")
	viper.SetDefault("clusterDiscoveryPort", "8000")
	viper.SetDefault("cacheDefaultExpirationTime", "300")

	//// look for a config file named oidc.[yaml|json] and read config from it
	//viper.SetConfigName("oidc")
	//viper.AddConfigPath("$HOME/.kube/")
	//viper.AddConfigPath(".")
	//err := viper.ReadInConfig()
	//if err != nil {
	//	log.Debug("No config file found! Using environment variables.")
	//}

	viper.SetEnvPrefix("oidc")
	viper.BindEnv("clientId")
	viper.BindEnv("clientSecret")
	viper.BindEnv("providerURL")
	viper.BindEnv("callbackURL")
	viper.BindEnv("listenAddress")
	viper.BindEnv("kubeCACertData")
	viper.BindEnv("kubeContextName")
	viper.BindEnv("kubeAPIAddress")
	viper.BindEnv("kubeClusterName")
	viper.BindEnv("clusterPeers")
	viper.BindEnv("clusterDiscoveryURL")
	viper.BindEnv("clusterDiscoveryScheme")
	viper.BindEnv("clusterDiscoveryPort")
	viper.BindEnv("clusterSelfURL")
	viper.BindEnv("KubeUserPrefix")
	viper.BindEnv("logLevel")
	viper.BindEnv("cacheDefaultExpirationTime")
}

func initialConfig(config *AppConfig) {
	config.ClientID = viper.GetString("clientId")
	config.ProviderURL = viper.GetString("providerURL")
	config.CallbackURL = viper.GetString("callbackURL")
	config.ListenAddress = viper.GetString("listenAddress")
	config.KubeCACertData = viper.GetString("kubeCACertData")
	config.KubeContextName = viper.GetString("kubeContextName")
	config.KubeClusterName = viper.GetString("kubeClusterName")
	config.KubeAPIAddress = viper.GetString("kubeAPIAddress")
	config.KubeUserPrefix = viper.GetString("KubeUserPrefix")
	config.ClusterPeers = strings.Split(viper.GetString("clusterPeers"), ",")
	config.LogLevel = viper.GetString("logLevel")
	config.ClusterSelfURL = viper.GetString("clusterSelfURL")
	config.ClusterDiscoveryScheme = viper.GetString("clusterDiscoveryScheme")
	config.ClusterDiscoveryPort = viper.GetString("clusterDiscoveryPort")
	config.CacheDefaultExpirationTime = viper.GetInt32("cacheDefaultExpirationTime")

	if viper.IsSet("clientSecret") {
		config.ClientSecret = viper.GetString("clientSecret")
	} else {
		logger.Panic("Could not determine clientSecret! Please make sure to set env variable OIDC_CLIENTSECRET.")
	}

	if !viper.IsSet("clusterPeers") && viper.IsSet("clusterDiscoveryURL") {
		config.ClusterDiscoveryURL = viper.GetString("clusterDiscoveryURL")
		if ok, err := IsValidURL(config.ClusterDiscoveryURL); !ok {
			logger.Panicf("Could not parse %s as valid URL: %s\n", config.ClusterDiscoveryURL, err)
		}
	} else if viper.IsSet("clusterPeers") {
		for _, u := range config.ClusterPeers {
			if ok, err := IsValidURL(u); !ok {
				logger.Panicf("Could not parse %s as valid URL: %s\n", u, err)
			}
		}
		logger.Infof("Loading cluster peers: %v\n", config.ClusterPeers)
	} else {
		logger.Info("No cluster config specified, starting in single instance mode.")
	}

	if ok, err := IsValidURL(config.CallbackURL); !ok {
		logger.Panicf("Could not parse %s as valid URL: %s\n", config.CallbackURL, err)
	} else {
		u, err := url.Parse(config.CallbackURL)
		if err != nil {
			logger.Panicf("Could not parse callback url %s: %s\n", config.CallbackURL, err)
		}
		config.CallbackPath = u.EscapedPath()
	}
}

func parseLogLevel(level string) log.Level {
	switch level {
	case "Debug":
		return log.DebugLevel
	case "Info":
		return log.InfoLevel
	case "Warn":
		return log.WarnLevel
	case "Error":
		return log.ErrorLevel
	default:
		logger.Println("Could not parse loglevel. Loglevel must be Debug, Info, Warn or Error! Defaulting to Info!")
		return log.InfoLevel
	}
}
