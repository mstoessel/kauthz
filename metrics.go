package main

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	oidcTokenRequestsTotal = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "oidc_token_requested_total",
			Help: "Total number of accesses on endpoints for requesting tokens.",
		})

	oidcTokensIssuedTotal = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "oidc_tokens_issued_total",
			Help: "Total number of tokens issued successfully.",
		})

	oidcClusterPeersTotal = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "oidc_cluster_peers",
			Help: "Number of cluster peers known to this instance.",
		})
)
