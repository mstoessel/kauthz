FROM alpine:3.8

RUN apk add --no-cache ca-certificates

ADD ./kauthz_linux-amd64  /kauthz

ENTRYPOINT ["/kauthz"]
