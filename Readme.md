# Kauthz Kubernetes OIDC Client

This is a service creating a HTTP endpoint for users to log in and request a JWT for the use in our Kubernetes clusters.

| Environment variable | Description | Default |
| --- | --- | --- |
| OIDC_PROVIDERURL | The URL for the OpenID Connect provider  | https://oidc.example.org |
| OIDC_CLIENTID | The oauth client id to use for accessing the OpenID Connect server (tested with dex) | k8s |
| OIDC_CLIENTSECRET | The secret used to connect to the OpenID Connect provider. No default is set |  |
| OIDC_CALLBACKURL | URL that the OpenID Connect provider uses to redirect back to the oidc client, including path| http://127.0.0.1:8000/auth/dex/callback |
| OIDC_LISTENADDRESS | TCP address the HTTP server listens on (golang format: ":8080" means all IPs on port 8080) | :8080 |
| OIDC_CLUSTERPEERS | Comma-separated list of URLs for all peers of the cluster. No default is set. This option takes priority over dynamic cluster peering with a discovery url (must not contain itself, e.g. "http://10.0.0.43:8080,http://oidc-client.example.org,https://bla.foo.org:8443") |  |
| OIDC_CLUSTERDISCOVERYURL | Valid domain with A records for every peer in the cluster. If set oidc-client will continuously look for new peers to include in the cluster. If none of the cluster discovery options is set, oidc-client will start as a single instance only. |  |
| OIDC_CLUSTERDISCOVERYSCHEME | Scheme to use when connecting to peers via the clusterdiscovery. Supports http and https | http |
| OIDC_CLUSTERDISCOVERYPORT | Port to use when connecting to peers via the clusterdiscovery. | 8000 |
| OIDC_KUBEAPIADDRESS | URL of the Kubernetes kube-apiserver, used for templating the kubeconfig | https://k8s.example.org |
| OIDC_KUBECONTEXTNAME | Name of the context in the kubeconfig, used only for templating | k8s |
| OIDC_KUBECLUSTERNAME | Name of the Kubernetes cluster in kubeconfig, used only for templating| kube_cluster |
| OIDC_KUBEUSERPREFIX | Prefix for the user config in the kubeconfig, used only for templating | oidc: |
| OIDC_KUBECACERTDATA | Kubernetes kube-apiserver public TLS cert base64 encoded pem | |
| OIDC_CACHEDEFAULTEXPIRATIONTIME | How long a individual OAuth state stays valid in seconds | 300 |
