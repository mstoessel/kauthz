package main

import (
	"github.com/coreos/go-oidc"
	"github.com/patrickmn/go-cache"
	"golang.org/x/oauth2"
	"gopkg.in/resty.v1"
)

// AppConfig is the global place for all application config parameters and structs
type AppConfig struct {
	ClientID                   string
	ClientSecret               string
	ProviderURL                string
	Provider                   *oidc.Provider
	Verifier                   *oidc.IDTokenVerifier
	OIDC                       *oidc.Config
	OAuth                      *oauth2.Config
	CallbackURL                string
	CallbackPath               string
	ListenAddress              string
	LogLevel                   string
	KubeCACertData             string
	KubeClusterName            string
	KubeContextName            string
	KubeAPIAddress             string
	KubeUserPrefix             string
	ClusterPeers               []string
	ClusterDiscoveryURL        string
	ClusterDiscoveryScheme     string
	ClusterDiscoveryPort       string
	ClusterSelfURL             string
	ClusterClients             []*resty.Client
	UseClusterMode             bool
	CacheDefaultExpirationTime int32
	LocalCache                 *cache.Cache
}

// Validation is the API object to check if a state parameter is in the cache of a cluster peer
type Validation struct {
	Exists bool   `json:"exists"`
	State  string `json:"state"`
}

// OAuthData is used for the oauth package methods
type OAuthData struct {
	Email      string `json:"email"`
	RawIDToken string
}

// TemplateData is used to template a kubeconfig
type TemplateData struct {
	Email      string
	IDToken    string
	Context    string
	Cluster    string
	CaCert     string
	APIAddress string
	UserPrefix string
}
