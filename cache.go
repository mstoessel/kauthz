package main

import (
	"github.com/patrickmn/go-cache"
	"time"
)

func newDefaultCache(config *AppConfig) *cache.Cache {
	return cache.New(time.Duration(config.CacheDefaultExpirationTime)*time.Second, 5*time.Minute)
}

func uuidExistsLocally(config *AppConfig, uuid string) bool {
	_, found := config.LocalCache.Get(uuid)
	return found
}
