module gitlab.com/mstoessel/kauthz

go 1.12

require (
	github.com/coreos/go-oidc v2.0.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pquerna/cachecontrol v0.0.0-20180517163645-1555304b9b35 // indirect
	github.com/prometheus/client_golang v0.9.2
	github.com/sirupsen/logrus v1.3.0
	github.com/spf13/viper v1.3.1
	golang.org/x/net v0.0.0-20190301231341-16b79f2e4e95
	golang.org/x/oauth2 v0.0.0-20190226205417-e64efc72b421
	gopkg.in/resty.v1 v1.12.0
	gopkg.in/square/go-jose.v2 v2.3.0 // indirect
)
