package main

import (
	"github.com/coreos/go-oidc"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
	"net/http"
	"os"
	"os/signal"
	"sync/atomic"
	"time"

	"golang.org/x/net/context"
)

var (
	appConfig *AppConfig
	healthy   int32
	ready     int32
	ctx       context.Context
	logger    *log.Logger
)

func init() {
	setupLog()

	prometheus.MustRegister(oidcClusterPeersTotal)
	prometheus.MustRegister(oidcTokenRequestsTotal)
	prometheus.MustRegister(oidcTokensIssuedTotal)
}

func main() {
	ctx = context.Background()
	var err error

	appConfig = CreateAppConfig()

	appConfig.Provider, err = oidc.NewProvider(ctx, appConfig.ProviderURL)
	if err != nil {
		logger.Panicf("Could not create new OIDC provider: %s", err)
	}
	appConfig.OIDC = &oidc.Config{
		ClientID: appConfig.ClientID,
	}
	appConfig.Verifier = appConfig.Provider.Verifier(appConfig.OIDC)

	appConfig.OAuth = &oauth2.Config{
		ClientID:     appConfig.ClientID,
		ClientSecret: appConfig.ClientSecret,
		Endpoint:     appConfig.Provider.Endpoint(),
		RedirectURL:  appConfig.CallbackURL,
		Scopes:       []string{oidc.ScopeOpenID, "profile", "email", "groups"},
	}

	logger.SetLevel(parseLogLevel(appConfig.LogLevel))

	router := http.NewServeMux()
	router.Handle("/", logging(logger, requestToken(appConfig)))
	router.Handle("/healthz", healthz())
	router.Handle("/ready", readiness())
	router.Handle(appConfig.CallbackPath, logging(logger, callback(appConfig)))
	router.Handle("/validate", logging(logger, validator(appConfig)))
	router.Handle("/kubeconfig", logging(logger, kubeconfig(appConfig)))
	router.Handle("/metrics", promhttp.Handler())

	server := &http.Server{
		Addr:         appConfig.ListenAddress,
		Handler:      router,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	// listen for OS interrupt signal and shutdown gracefully
	go func() {
		<-quit
		logger.Println("Server is shutting down...")
		atomic.StoreInt32(&healthy, 0)

		ctx, cancel := context.WithTimeout(context.Background(), 16*time.Second)
		defer cancel()

		server.SetKeepAlivesEnabled(false)
		if err := server.Shutdown(ctx); err != nil {
			logger.Fatalf("Could not gracefully shutdown server: %s\n", err)
		}

		close(done)
	}()

	logger.Printf("listening on http://%s/\n", appConfig.ListenAddress)
	atomic.StoreInt32(&healthy, 1)

	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		logger.Fatalf("Could not listen on %s: %s\n", appConfig.ListenAddress, err)
	}

	<-done
	logger.Println("Server stopped gracefully.")
}
