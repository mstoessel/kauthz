package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"net/http"
	"net/url"
	"sync/atomic"
	"text/template"
)

func requestToken(config *AppConfig) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		state := uuid.New().String()
		http.Redirect(w, r, config.OAuth.AuthCodeURL(state), http.StatusFound)
		config.LocalCache.Set(state, "", 0)
		oidcTokenRequestsTotal.Inc()
	})
}

func healthz() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if atomic.LoadInt32(&healthy) == 1 {
			w.WriteHeader(http.StatusOK)
			fmt.Fprintln(w, "OK")
			return
		}
		w.WriteHeader(http.StatusServiceUnavailable)
	})
}

func readiness() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if atomic.LoadInt32(&ready) == 1 {
			w.WriteHeader(http.StatusOK)
			fmt.Fprintln(w, "READY")
			return
		}
	})
}

func callback(config *AppConfig) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		state := r.URL.Query().Get("state")

		if !uuidExistsLocally(config, state) && config.UseClusterMode {
			if exists, err := uuidExistsClusterWide(config, state); !exists {
				http.Error(w, "state did not match", http.StatusBadRequest)
				logger.Errorf("Request state '%s' did not match any state '%s'!\n", r.URL.Query().Get("state"), state)
				return
			} else if err != nil {
				http.Error(w, "Could not find matching OAuth state!", http.StatusBadRequest)
				logger.Errorf("Error occurred while validating OAuth state in cluster: %s\n", err)
				return
			}

		} else if !uuidExistsLocally(config, state) && !config.UseClusterMode {
			http.Error(w, "state did not match", http.StatusBadRequest)
			logger.Errorf("Request state '%s' did not match any state '%s'!\n", r.URL.Query().Get("state"), state)
		}

		oauth2Token, err := config.OAuth.Exchange(ctx, r.URL.Query().Get("code"))
		if err != nil {
			http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
			logger.Errorf("Failed to exchange token! %s\n", err)
			return
		}
		rawIDToken, ok := oauth2Token.Extra("id_token").(string)
		if !ok {
			http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)
			logger.Errorf("No id_token field in oauth2 token!\n")
			return
		}
		idToken, err := config.Verifier.Verify(ctx, rawIDToken)
		if err != nil {
			http.Error(w, "Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)
			logger.Errorf("Failed to verify ID token! %s\n", err)
			return
		}

		oauth2Token.AccessToken = "*REDACTED*"

		var oauthData OAuthData

		if err := idToken.Claims(&oauthData); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			logger.Errorf("Cannot unmarshal claims from token! %s\n", err)
			return
		}
		oauthData.RawIDToken = rawIDToken

		u, err := url.Parse(config.CallbackURL)
		u.Path = "/kubeconfig"
		u.RawQuery = "email=" + oauthData.Email + "&token=" + oauthData.RawIDToken
		data := rawIDToken + "\n\n# Get your Kubernetes config file here: \n# " + u.String()

		w.Header().Add("Content-Type", "text/plain")
		w.Write([]byte(data))
		oidcTokensIssuedTotal.Inc()
	})
}

func validator(config *AppConfig) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		uuid := r.URL.Query().Get("state")
		if uuid == "" {
			http.Error(w, "You must specify tbe OAuth state ID with the key 'state'.", http.StatusBadRequest)
			logger.Errorf("Could not validate with following state query: %v\n", uuid)
			return
		}

		data := Validation{}

		if uuidExistsLocally(config, uuid) {
			data.Exists = true
			data.State = uuid
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
		} else {
			data.Exists = false
			data.State = uuid
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusNotFound)
		}

		json.NewEncoder(w).Encode(data)
	})
}

// kubeconfig() assembles a kubeconfig json by using an email and a token passed as request query parameters
func kubeconfig(config *AppConfig) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query()

		email := q.Get("email")
		if email == "" {
			http.Error(w, "Parameter 'email' is missing!", http.StatusInternalServerError)
			logger.Errorf("Parameter 'email' is missing!\n")
			return
		}

		token := q.Get("token")
		if token == "" {
			http.Error(w, "Parameter 'token' is missing!", http.StatusInternalServerError)
			logger.Errorf("Parameter 'token' is missing!\n")
			return
		}

		rawconfig := `
{
	"apiVersion": "v1",
	"kind": "Config",
	"current-context": "{{.Context}}",
	"clusters": [
	  {
		"name": "{{.Cluster}}",
		"cluster": {
		  "certificate-authority-data": "{{.CaCert}}",
		  "server": "{{.APIAddress}}"
		}
	  }
	],
	"contexts": [
	  {
		"name": "{{.Context}}",
		"context": {
		  "cluster": "{{.Cluster}}",
		  "user": "{{.UserPrefix}}{{.Email}}"
		}
	  }
	],
	"preferences": {
	  "colors": true
	},
	"users": [
	  {
		"name": "{{.UserPrefix}}{{.Email}}",
		"user": {
		  "token": "{{.IDToken}}"
		}
	  }
	]
}
`
		templateData := TemplateData{
			Context:    config.KubeContextName,
			APIAddress: config.KubeAPIAddress,
			Cluster:    config.KubeClusterName,
			CaCert:     config.KubeCACertData,
			UserPrefix: config.KubeUserPrefix,
			Email:      email,
			IDToken:    token,
		}

		t := template.Must(template.New("configTemplate").Parse(rawconfig))
		var configBuffer bytes.Buffer
		err := t.Execute(&configBuffer, templateData)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			logger.Errorf("Cannot execute kubeconfig template! %s\n", err)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(configBuffer.Bytes())
	})
}

// logging() functions as a logging middleware used to log http request details by passing itself as a http.HandlerFunc
// to a ServeMux and passing a Logger instance and the actual http.Handler to becalled as parameters
func logging(logger *log.Logger, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			logger.Debugln(r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent())
		}()
		next.ServeHTTP(w, r)
	})
}
