package main

import (
	"errors"
	"gopkg.in/resty.v1"
	"net"
	"net/http"
	"net/url"
	"sync/atomic"
	"time"
)

func setupClusterPeers(config *AppConfig) error {

	// statically set cluster peers take precedence over discovered peers
	if len(config.ClusterPeers) > 0 {
		config.ClusterClients = make([]*resty.Client, len(config.ClusterPeers))
		for i, peer := range config.ClusterPeers {
			u, err := url.Parse(peer)
			if err != nil {
				logger.Errorf("Could not parse %s as valid URL, defaulting to single instance mode: %s", peer, err)
				return err
			}

			config.ClusterClients[i] = newDefaultClient()
			config.ClusterClients[i].SetHostURL(u.Scheme + "://" + u.Host)
		}

		logger.Infof("Using statically configured peers: %v\n", config.ClusterPeers)
		atomic.StoreInt32(&ready, 1)
		return nil
	}

	peers, err := discoverPeers(config.ClusterDiscoveryURL)
	if err != nil {
		logger.Errorf("Could not resolve %s, defaulting to single instance mode: %s\n", config.ClusterDiscoveryURL, err)
		return err
	}

	if len(peers) <= 0 {
		logger.Infof("No peers could be discovered under %s, defaulting to single instance mode\n", config.ClusterDiscoveryURL)
		return errors.New("no peers discovered")
	}

	config.ClusterClients = make([]*resty.Client, len(peers))

	for i, peer := range peers {
		config.ClusterClients[i] = newDefaultClient()
		config.ClusterClients[i].SetHostURL(config.ClusterDiscoveryScheme + "://" + peer + ":" + config.ClusterDiscoveryPort)
	}

	logger.Infof("Discovered peers: %v\nUsing scheme: %s\nUsing port: %s\n", peers, config.ClusterDiscoveryScheme, config.ClusterDiscoveryPort)
	atomic.StoreInt32(&ready, 1)
	return nil
}

func uuidExistsClusterWide(config *AppConfig, state string) (bool, error) {
	var resultError error
	for _, client := range config.ClusterClients {
		resp, err := client.R().SetQueryParam("state", state).Get("/validate")
		if err != nil && resp.StatusCode() != http.StatusNotFound {
			logger.Errorf("Could not validate %s against %s: %s\n", state, client.HostURL, err)
			resultError = err
			continue
		} else if resp.StatusCode() == http.StatusNotFound {
			logger.Debugf("Did not find state %s on host %s\n", state, client.HostURL)
			continue
		} else if resp.StatusCode() == http.StatusOK {
			logger.Debugf("Found state %s on host %s\n", state, client.HostURL)
			return true, nil
		}
	}
	return false, resultError
}

func newDefaultClient() *resty.Client {
	client := resty.New()
	client.SetRetryCount(10)
	client.SetRetryWaitTime(2 * time.Second)
	client.SetRetryMaxWaitTime(5 * time.Second)
	client.SetHeaders(map[string]string{
		"Content-Type": "application/json",
		"Accept":       "application/json",
	})

	return client
}

func discoverPeers(u string) ([]string, error) {
	host, err := url.Parse(u)
	if err != nil {
		logger.Errorf("Could not parse %s as valid URL: %s\n", u, err)
		return make([]string, 0), err
	}

	addr, err := net.LookupHost(host.Host)
	if err != nil {
		return make([]string, 0), err
	}

	return addr, nil
}
